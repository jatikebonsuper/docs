# QA issue generation

Manual QA is performed on changes that have been introduced between versions.

The QA generation task can be used to create an issue that lists the Merge Requests associated with merge commits introduced between two references.

Each QA task should be linked as a related issue to the main ~"Monthly Release" issue.

For example:
- A QA issue for a patch release would list changes between tags `v10.8.1` and `v10.8.2`
- A QA issue for a new monthly release would list changes between the previous release's stable branch `10-8-stable` and the latest RCs tag `v11.0.0-rc1`

> Note: You can verify the output of the generated QA task by doing a dry-run using `TEST=true`. Please see the [Rake task docs][rake-task-doc]

## Create an issue to track the QA tasks

To ensure that manual QA for changes is carried out we create a task which generates a checklist of changes

First, make sure you have [release-tools](https://gitlab.com/gitlab-org/release-tools)
cloned locally, and [set it up](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/rake-tasks.md#setup)

### Monthly or patch release

**A new issue should be created for each new RC.**

1. Create the issue using [the `qa_issue` task][qa_issue-task-doc] in the `release-tools` project:

### Security release

**A new issue should be created for each backported versions as well.**

1. Create the issue using [the `security_qa_issue` task][security_qa_issue-task-doc] in the `release-tools` project:

[Return to Guides](../README.md#guides)

[qa_issue-task-doc]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc/rake-tasks.md#qa_issuefromtoversion
[security_qa_issue-task-doc]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc/rake-tasks.md#security_qa_issuefromtoversion
