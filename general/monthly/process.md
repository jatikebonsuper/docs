# Monthly release general process

The release process follows [this set of templates](https://gitlab.com/gitlab-org/release-tools/tree/master/templates)

## First RC (RC1)
* Automated tests are run against Staging.
* A QA Issue is automatically created listing the changes for the release candidate
  * Changes are displayed as Merge Requests and are collected via a comparison of this RC against the previous stable release, associated with the Merge Request author.
  * The Engineering teams confirms that these other changes come with tests or have been tested on staging. 
  * Regressions and bugs are raised and resolved.
  * Example: [11.6.0-rc1 QA Issue](https://gitlab.com/gitlab-org/release/tasks/issues/576)
* After 24 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed to deploy to production.

## Subsequent RCs
* Automated tests are run against Staging.
* A QA issue is automatically created listing the changes for the release candidate
  * The Merge Requests are merged since the previous release candidate are captured in the QA issue, associated with the Merge Request author.
  * The engineer performs testing on staging, or confirms that the change came with automated tests that are passing.
  * Regressions and bugs are raised and resolved.
  * Example: [11.6.0-rc4 QA Issue](https://gitlab.com/gitlab-org/release/tasks/issues/589)
* After 12 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed to deploy to production.

## Final Release
* The process is the same as the **Subsequent RCs**.
* Automated tests are run against Canary as a sanity check.
* The final release candidate is deployed to production.